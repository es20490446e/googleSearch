```
USAGE
       googleSearch [query]

       googleSearch site:[site] [query]

TERMS
       The usage of this tool requires compliance with Google's Terms of Service.

       In particular using it for high frequency commercial queries requires their permission.

       The developers of this software aren't affiliated with Google.
```
